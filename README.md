# Macbook Build Script

This is a script that I use with a fresh install of OS X to get all of my preferred applications installed with ease.

Feel free to use at your own risk or customize to your liking. 

## Instructions

1. Install Homebrew. This also installs Command Line Tools which usually takes awhile.

        /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

2. Add Homebrew to your path.

        (echo; echo 'eval "$(/opt/homebrew/bin/brew shellenv)"') >> $HOME/.zprofile
        eval "$(/opt/homebrew/bin/brew shellenv)"

3. Install Oh-My-ZSH.

        sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

4. Run the build script.

        /bin/bash -c "$(curl -fSL https://gitlab.com/api/v4/projects/31482487/repository/files/step1-build.sh/raw\?ref=master)"

        /bin/bash -c "$(curl -fSL https://gitlab.com/api/v4/projects/31482487/repository/files/step2-apps.sh/raw\?ref=master)"

5. Perform additional configuration steps below.

6. Run the Laravel Dotfiles

        /bin/bash -c "$(curl -fSL https://gitlab.com/api/v4/projects/53652396/repository/files/install.sh/raw\?ref=main)"

## Additional Configuration Steps for Applications

1. Open Dropbox and sign in to start the syncing process. Configure selective sync if needed.
2. Sign in with Google account(s). Bookmarks will auto-sync.
3. Open 1Password and sign in.
4. Open Spotify and sign in. Start rocking out!
5. Open VSCode and sign in to settings sync.
 
## Additional Configuration Steps for Mac OS

* System Preferences > General
    * Apperance: Dark
    * Accent Color: Purple
    * Default web browser: Chrome
* System Preferences > Desktop & Screensaver
    * Configure whatever you feel like
* System Preferences > Dock
    * Size: 15-20%
    * Magnification: 30-40%
    * Show recent applications in Dock: Disabled
* System Preferences > Security & Privacy
    * Require Password: 5 Minutes
    * Allow apps downloaded from: App Store and identified developers
    * Filevault: Enabled
    * Firewall: Enabled
* System Preferences > Energy Saver
    * Battery: 10 Minutes
    * Power Adapter: 20 Minutes
    * Powernap: Disabled
* System Preferences > Keyboard
    * Shortcuts > Mission Controller > Move left a space (Cmd+Left Arrow)
    * Shortcuts > Mission Controller > Move right a space (Cmd+Right Arrow)
* System Preferences > Mouse
    * Tracking Speed: 70%
    * Scrolling Speed: 40%
* System Preferences > Trackpad
    * Secondary click: Click with two fingers
    * Click: Medium
    * Tracking Speed: 60%
    * Scroll direction Natural: Disabled
* System Preferences 


#!/bin/bash

# Build Script for Macbook Configuration
# gitlab.com/jeffersonmartin/dotfiles
# 
# Usage
# ~/Downloads/dotfiles-master/build.sh

# Define Variables for Syntax Highlighting Colors
C_DEFAULT='\033[m'
C_WHITE='\033[1m'
C_BLACK='\033[30m'
C_RED='\033[31m'
C_GREEN='\033[32m'
C_YELLOW='\033[33m'
C_BLUE='\033[34m'
C_PURPLE='\033[35m'
C_CYAN='\033[36m'
C_LIGHTGRAY='\033[37m'
C_DARKGRAY='\033[1;30m'
C_LIGHTRED='\033[1;31m'
C_LIGHTGREEN='\033[1;32m'
C_LIGHTYELLOW='\033[1;33m'
C_LIGHTBLUE='\033[1;34m'
C_LIGHTPURPLE='\033[1;35m'
C_LIGHTCYAN='\033[1;36m'

#
# Output - Script Header
#

printf "${C_PURPLE}\n"
printf "   /**\n"
printf "    *\n"
printf "    ${C_PURPLE}*   ${C_PURPLE}Build Script for Macbook Configuration\n"
printf "    ${C_PURPLE}*   ${C_PURPLE}Step 2 - Applications\n"
printf "    ${C_PURPLE}*   \n"
printf "    ${C_PURPLE}*   ${C_DEFAULT}@package${C_PURPLE} gitlab.com/jeffersonmartin/dotfiles\n"
printf "    ${C_PURPLE}*   ${C_DEFAULT}@author${C_PURPLE}  Jeff Martin\n"
printf "    ${C_PURPLE}*   \n"
printf "    */\n"
printf "    \n${C_DEFAULT}"

# Ask for the administrator password upfront
sudo -v

# Keep-alive: update existing `sudo` time stamp until `.macos` has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

printf "\n\n${C_CYAN}Updating Brew Package Manager\n"
brew update 

printf "\n\n${C_CYAN}Installing Desktop Applications\n"
brew install \
	1password \
	1password-cli \
	bartender \ 
	clocker \
	dbngin \
	fork \
	google-chrome \
	google-drive \
	istat-menus \ 
	iterm2 \
	nordlayer \
	obsidian \
	postman \
	rectangle \
    slack \
	spotify \
	sublime-text \
    tableplus \
	tinkerwell \
	topnotch \
	visual-studio-code \
	ykman \
	zoom

printf "\n\n${C_CYAN}Adding Application Icons to Dock\n"

killall Dock

# Wipe all (default) app icons from the Dock
defaults write com.apple.dock persistent-apps -array
killall Dock

# Add installed applications to Dock
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/System/Applications/Launchpad.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/1Password.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Google Chrome.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Slack.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/zoom.us.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/System/Applications/Messages.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Visual Studio Code.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/Fork.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Applications/iTerm.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
defaults write com.apple.dock persistent-apps -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/System/Applications/System Settings.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>'
killall Dock

printf "\n\n${C_LIGHTGREEN}App Installation complete. You can open each application to perform the initial configuration.${C_DEFAULT}\n"
printf "\n"


#!/bin/bash

# Build Script for Macbook Configuration
# gitlab.com/jeffersonmartin/dotfiles
# 
# Usage
# ~/Downloads/dotfiles-master/build.sh

# Define Variables for Syntax Highlighting Colors
C_DEFAULT='\033[m'
C_WHITE='\033[1m'
C_BLACK='\033[30m'
C_RED='\033[31m'
C_GREEN='\033[32m'
C_YELLOW='\033[33m'
C_BLUE='\033[34m'
C_PURPLE='\033[35m'
C_CYAN='\033[36m'
C_LIGHTGRAY='\033[37m'
C_DARKGRAY='\033[1;30m'
C_LIGHTRED='\033[1;31m'
C_LIGHTGREEN='\033[1;32m'
C_LIGHTYELLOW='\033[1;33m'
C_LIGHTBLUE='\033[1;34m'
C_LIGHTPURPLE='\033[1;35m'
C_LIGHTCYAN='\033[1;36m'

#
# Output - Script Header
#

printf "${C_PURPLE}\n"
printf "   /**\n"
printf "    *\n"
printf "    ${C_PURPLE}*   ${C_PURPLE}Build Script for Macbook Configuration\n"
printf "    ${C_PURPLE}*   ${C_PURPLE}Step 1 - Build and Compile Dependencies\n"
printf "    ${C_PURPLE}*   \n"
printf "    ${C_PURPLE}*   ${C_DEFAULT}@package${C_PURPLE} gitlab.com/jeffersonmartin/dotfiles\n"
printf "    ${C_PURPLE}*   ${C_DEFAULT}@author${C_PURPLE}  Jeff Martin\n"
printf "    ${C_PURPLE}*   \n"
printf "    */\n"
printf "    \n${C_DEFAULT}"

# Ask for the administrator password upfront
sudo -v

# Keep-alive: update existing `sudo` time stamp until `.macos` has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

printf "\n${C_CYAN}Updating Brew Package Manager${C_DEFAULT}\n"
brew update 
brew upgrade

printf "\n${C_CYAN}Adding SSH Configuration for 1Password Agent and CLI${C_DEFAULT}\n"
brew install 1password-cli
mkdir $HOME/.ssh
curl https://gitlab.com/jeffersonmartin/m1-dotfiles/-/raw/master/ssh_config -o $HOME/.ssh/config

printf "\n${C_CYAN}Install ZSH Dependencies${C_DEFAULT}\n"
brew install autojump fzf wget grep
curl https://gitlab.com/jeffersonmartin/m1-dotfiles/-/raw/master/.zshrc -o $HOME/.zshrc
source ~/.zshrc

printf "\n${C_CYAN}Configuring MacOS with Sensible Defaults${C_DEFAULT}\n"

# Close any open System Preferences panes, to prevent them from overriding
# settings we’re about to change
osascript -e 'tell application "System Preferences" to quit'

printf "${C_CYAN}Security: Enable Firewall${C_DEFAULT}\n"
sudo /usr/libexec/ApplicationFirewall/socketfilterfw --setglobalstate on

###############################################################################
# General UI/UX                                                               #
###############################################################################

printf "${C_CYAN}General: Disable the sound effects on boot${C_DEFAULT}\n"
sudo nvram SystemAudioVolume=" "

printf "${C_CYAN}General: Save to disk (not to iCloud) by default${C_DEFAULT}\n"
defaults write NSGlobalDomain NSDocumentSaveNewDocumentsToCloud -bool false

printf "${C_CYAN}General: Disable the Are you sure you want to open this application? dialog${C_DEFAULT}\n"
defaults write com.apple.LaunchServices LSQuarantine -bool false

printf "${C_CYAN}General: Disable the crash reporter${C_DEFAULT}\n"
#defaults write com.apple.CrashReporter DialogType -string "none"

printf "${C_CYAN}General: Reveal IP address, hostname, OS version, etc. when clicking the clock in the login window${C_DEFAULT}\n"
sudo defaults write /Library/Preferences/com.apple.loginwindow AdminHostInfo HostName

printf "${C_CYAN}General: Disable automatic capitalization as it’s annoying when typing code${C_DEFAULT}\n"
defaults write NSGlobalDomain NSAutomaticCapitalizationEnabled -bool false

printf "${C_CYAN}General: Disable smart dashes as they’re annoying when typing code${C_DEFAULT}\n"
defaults write NSGlobalDomain NSAutomaticDashSubstitutionEnabled -bool false

printf "${C_CYAN}General: Disable automatic period substitution as it’s annoying when typing code${C_DEFAULT}\n"
defaults write NSGlobalDomain NSAutomaticPeriodSubstitutionEnabled -bool false

printf "${C_CYAN}General: Disable smart quotes as they’re annoying when typing code${C_DEFAULT}\n"
defaults write NSGlobalDomain NSAutomaticQuoteSubstitutionEnabled -bool false

printf "${C_CYAN}General: Disable auto-correct${C_DEFAULT}\n"
defaults write NSGlobalDomain NSAutomaticSpellingCorrectionEnabled -bool false

# Set a custom wallpaper image. `DefaultDesktop.jpg` is already a symlink, and
# all wallpapers are in `/Library/Desktop Pictures/`. The default is `Wave.jpg`.
#rm -rf ~/Library/Application Support/Dock/desktoppicture.db
#sudo rm -rf /System/Library/CoreServices/DefaultDesktop.jpg
#sudo ln -s /path/to/your/image /System/Library/CoreServices/DefaultDesktop.jpg

###############################################################################
# Trackpad, mouse, keyboard, Bluetooth accessories, and input                 #
###############################################################################

printf "${C_CYAN}Trackpad: enable tap to click for this user and for the login screen${C_DEFAULT}\n"
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1

printf "${C_CYAN}Trackpad: map bottom right corner to right-click${C_DEFAULT}\n"
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadCornerSecondaryClick -int 2
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadRightClick -bool true
defaults -currentHost write NSGlobalDomain com.apple.trackpad.trackpadCornerClickBehavior -int 1
defaults -currentHost write NSGlobalDomain com.apple.trackpad.enableSecondaryClick -bool true

printf "${C_CYAN}Trackpad: Disable “natural” scrolling${C_DEFAULT}\n"
defaults write NSGlobalDomain com.apple.swipescrolldirection -bool false

###############################################################################
# Energy saving                                                               #
###############################################################################

printf "${C_CYAN}Energy: Enable lid wakeup${C_DEFAULT}\n"
sudo pmset -a lidwake 1

printf "${C_CYAN}Energy: Sleep the display after 15 minutes${C_DEFAULT}\n"
sudo pmset -a displaysleep 15

printf "${C_CYAN}Energy: Set machine sleep to 5 minutes on battery${C_DEFAULT}\n"
sudo pmset -b sleep 5

printf "${C_CYAN}Energy: Require password immediately after sleep or screen saver begins${C_DEFAULT}\n"
defaults write com.apple.screensaver askForPassword -int 1
defaults write com.apple.screensaver askForPasswordDelay -int 0

###############################################################################
# Screen                                                                      #
###############################################################################

printf "${C_CYAN}Screenshots: Save screenshots to the documents screenshots folder${C_DEFAULT}\n"
mkdir $HOME/Documents/Screenshots
defaults write com.apple.screencapture location -string "$HOME/Documents/Screenshots"

printf "${C_CYAN}Screnshots: Save screenshots in PNG format (other options: BMP, GIF, JPG, PDF, TIFF)${C_DEFAULT}\n"
defaults write com.apple.screencapture type -string "png"

printf "${C_CYAN}Screnshots: Disable shadow in screenshots${C_DEFAULT}\n"
defaults write com.apple.screencapture disable-shadow -bool true

###############################################################################
# Finder                                                                      #
###############################################################################

printf "${C_CYAN}Finder: show hidden files by default${C_DEFAULT}\n"
#defaults write com.apple.finder AppleShowAllFiles -bool true

printf "${C_CYAN}Finder: show all filename extensions${C_DEFAULT}\n"
defaults write NSGlobalDomain AppleShowAllExtensions -bool true

printf "${C_CYAN}Finder: show status bar${C_DEFAULT}\n"
defaults write com.apple.finder ShowStatusBar -bool true

printf "${C_CYAN}Finder: show path bar${C_DEFAULT}\n"
defaults write com.apple.finder ShowPathbar -bool true

printf "${C_CYAN}Finder: Display full POSIX path as Finder window title${C_DEFAULT}\n"
defaults write com.apple.finder _FXShowPosixPathInTitle -bool true

printf "${C_CYAN}Finder: Keep folders on top when sorting by name${C_DEFAULT}\n"
defaults write com.apple.finder _FXSortFoldersFirst -bool true

printf "${C_CYAN}Finder: When performing a search, search the current folder by default${C_DEFAULT}\n"
defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

printf "${C_CYAN}Finder: Disable the warning when changing a file extension${C_DEFAULT}\n"
defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false

printf "${C_CYAN}Finder: Enable spring loading for directories${C_DEFAULT}\n"
defaults write NSGlobalDomain com.apple.springing.enabled -bool true

printf "${C_CYAN}Finder: Remove the spring loading delay for directories${C_DEFAULT}\n"
defaults write NSGlobalDomain com.apple.springing.delay -float 0

printf "${C_CYAN}Finder: Avoid creating .DS_Store files on network or USB volumes${C_DEFAULT}\n"
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true
defaults write com.apple.desktopservices DSDontWriteUSBStores -bool true

###############################################################################
# Dock, Dashboard, and hot corners                                            #
###############################################################################

printf "${C_CYAN}Dock: Enable highlight hover effect for the grid view of a stack (Dock)${C_DEFAULT}\n"
defaults write com.apple.dock mouse-over-hilite-stack -bool true

printf "${C_CYAN}Dock: Set the icon size of Dock items${C_DEFAULT}\n"
defaults write com.apple.dock tilesize -int 55

printf "${C_CYAN}Dock: Set the icon size of Dock items that have been magnified (on hover)${C_DEFAULT}\n"
defaults write com.apple.dock largesize -int 80
defaults write com.apple.dock magnification -bool true

printf "${C_CYAN}Dock: Change minimize/maximize window effect${C_DEFAULT}\n"
defaults write com.apple.dock mineffect -string "scale"

printf "${C_CYAN}Dock: Minimize windows into their application’s icon${C_DEFAULT}\n"
defaults write com.apple.dock minimize-to-application -bool true

printf "${C_CYAN}Dock: Enable spring loading for all Dock items${C_DEFAULT}\n"
defaults write com.apple.dock enable-spring-load-actions-on-all-items -bool true

printf "${C_CYAN}Dock: Show only open applications in the Dock${C_DEFAULT}\n"
#defaults write com.apple.dock static-only -bool true

printf "${C_CYAN}Dock: Don’t animate opening applications from the Dock${C_DEFAULT}\n"
defaults write com.apple.dock launchanim -bool false

printf "${C_CYAN}Dock: Don’t show recent applications in Dock${C_DEFAULT}\n"
defaults write com.apple.dock show-recents -bool false

printf "${C_CYAN}Mission Control: Speed up Mission Control animations${C_DEFAULT}\n"
defaults write com.apple.dock expose-animation-duration -float 0.1

printf "${C_CYAN}Mission Control: Don’t group windows by application in Mission Control${C_DEFAULT}\n"
# (i.e. use the old Exposé behavior instead)
defaults write com.apple.dock expose-group-by-app -bool false

printf "${C_CYAN}Mission Control: Disable Dashboard${C_DEFAULT}\n"
defaults write com.apple.dashboard mcx-disabled -bool true

printf "${C_CYAN}Mission Control: Don’t show Dashboard as a Space${C_DEFAULT}\n"
defaults write com.apple.dock dashboard-in-overlay -bool true

printf "${C_CYAN}Mission Control: Don’t automatically rearrange Spaces based on most recent use${C_DEFAULT}\n"
defaults write com.apple.dock mru-spaces -bool false

printf "${C_CYAN}Mission Control: Hot corners${C_DEFAULT}\n"
# Possible values:
#  0: no-op
#  2: Mission Control
#  3: Show application windows
#  4: Desktop
#  5: Start screen saver
#  6: Disable screen saver
#  7: Dashboard
# 10: Put display to sleep
# 11: Launchpad
# 12: Notification Center
# 13: Lock Screen
# Top left screen corner → Mission Control
defaults write com.apple.dock wvous-tl-corner -int 2
defaults write com.apple.dock wvous-tl-modifier -int 0
# Top right screen corner → Desktop
defaults write com.apple.dock wvous-tr-corner -int 4
defaults write com.apple.dock wvous-tr-modifier -int 0
# Bottom left screen corner → Start screen saver
defaults write com.apple.dock wvous-bl-corner -int 5
defaults write com.apple.dock wvous-bl-modifier -int 0

###############################################################################
# Safari & WebKit                                                             #
###############################################################################

printf "${C_CYAN}Safari: don’t send search queries to Apple${C_DEFAULT}\n"
defaults write com.apple.Safari UniversalSearchEnabled -bool false
defaults write com.apple.Safari SuppressSearchSuggestions -bool true

printf "${C_CYAN}Safari: Press Tab to highlight each item on a web page${C_DEFAULT}\n"
defaults write com.apple.Safari WebKitTabToLinksPreferenceKey -bool true
defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2TabsToLinks -bool true

printf "${C_CYAN}Safari: Show the full URL in the address bar (note: this still hides the scheme)${C_DEFAULT}\n"
defaults write com.apple.Safari ShowFullURLInSmartSearchField -bool true

printf "${C_CYAN}Safari: Prevent Safari from opening ‘safe’ files automatically after downloading${C_DEFAULT}\n"
defaults write com.apple.Safari AutoOpenSafeDownloads -bool false

printf "${C_CYAN}Safari: Allow hitting the Backspace key to go to the previous page in history${C_DEFAULT}\n"
defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2BackspaceKeyNavigationEnabled -bool true

printf "${C_CYAN}Safari: Hide Safari’s bookmarks bar by default${C_DEFAULT}\n"
defaults write com.apple.Safari ShowFavoritesBar -bool false

printf "${C_CYAN}Safari: Hide Safari’s sidebar in Top Sites${C_DEFAULT}\n"
defaults write com.apple.Safari ShowSidebarInTopSites -bool false

printf "${C_CYAN}Safari: Disable Safari’s thumbnail cache for History and Top Sites${C_DEFAULT}\n"
defaults write com.apple.Safari DebugSnapshotsUpdatePolicy -int 2

printf "${C_CYAN}Safari: Enable Safari’s debug menu${C_DEFAULT}\n"
defaults write com.apple.Safari IncludeInternalDebugMenu -bool true

printf "${C_CYAN}Safari: Make Safari’s search banners default to Contains instead of Starts With${C_DEFAULT}\n"
defaults write com.apple.Safari FindOnPageMatchesWordStartsOnly -bool false

printf "${C_CYAN}Safari: Remove useless icons from Safari’s bookmarks bar${C_DEFAULT}\n"
defaults write com.apple.Safari ProxiesInBookmarksBar "()"

printf "${C_CYAN}Safari: Enable the Develop menu and the Web Inspector in Safari${C_DEFAULT}\n"
defaults write com.apple.Safari IncludeDevelopMenu -bool true
defaults write com.apple.Safari WebKitDeveloperExtrasEnabledPreferenceKey -bool true
defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2DeveloperExtrasEnabled -bool true

printf "${C_CYAN}Safari: Add a context menu item for showing the Web Inspector in web views${C_DEFAULT}\n"
defaults write NSGlobalDomain WebKitDeveloperExtras -bool true

printf "${C_CYAN}Safari: Enable continuous spellchecking${C_DEFAULT}\n"
defaults write com.apple.Safari WebContinuousSpellCheckingEnabled -bool true

printf "${C_CYAN}Safari: Disable auto-correct${C_DEFAULT}\n"
defaults write com.apple.Safari WebAutomaticSpellingCorrectionEnabled -bool false

printf "${C_CYAN}Safari: Disable AutoFill${C_DEFAULT}\n"
defaults write com.apple.Safari AutoFillFromAddressBook -bool false
defaults write com.apple.Safari AutoFillPasswords -bool false
defaults write com.apple.Safari AutoFillCreditCardData -bool false
defaults write com.apple.Safari AutoFillMiscellaneousForms -bool false

printf "${C_CYAN}Safari: Warn about fraudulent websites${C_DEFAULT}\n"
defaults write com.apple.Safari WarnAboutFraudulentWebsites -bool true

printf "${C_CYAN}Safari: Disable plug-ins${C_DEFAULT}\n"
defaults write com.apple.Safari WebKitPluginsEnabled -bool false
defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2PluginsEnabled -bool false

printf "${C_CYAN}Safari: Disable Java${C_DEFAULT}\n"
defaults write com.apple.Safari WebKitJavaEnabled -bool false
defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2JavaEnabled -bool false
defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2JavaEnabledForLocalFiles -bool false

printf "${C_CYAN}Safari: Block pop-up windows${C_DEFAULT}\n"
defaults write com.apple.Safari WebKitJavaScriptCanOpenWindowsAutomatically -bool false
defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2JavaScriptCanOpenWindowsAutomatically -bool false

printf "${C_CYAN}Safari: Disable auto-playing video${C_DEFAULT}\n"
#defaults write com.apple.Safari WebKitMediaPlaybackAllowsInline -bool false
#defaults write com.apple.SafariTechnologyPreview WebKitMediaPlaybackAllowsInline -bool false
#defaults write com.apple.Safari com.apple.Safari.ContentPageGroupIdentifier.WebKit2AllowsInlineMediaPlayback -bool false
#defaults write com.apple.SafariTechnologyPreview com.apple.Safari.ContentPageGroupIdentifier.WebKit2AllowsInlineMediaPlayback -bool false

printf "${C_CYAN}Safari: Enable Do Not Track${C_DEFAULT}\n"
defaults write com.apple.Safari SendDoNotTrackHTTPHeader -bool true

printf "${C_CYAN}Safari: Update extensions automatically${C_DEFAULT}\n"
defaults write com.apple.Safari InstallExtensionUpdatesAutomatically -bool true

###############################################################################
# Terminal & iTerm 2                                                          #
###############################################################################

printf "${C_CYAN}Terminal: Only use UTF-8 in Terminal.app${C_DEFAULT}\n"
defaults write com.apple.terminal StringEncodings -array 4

# printf "${C_CYAN}Use a modified version of the Solarized Dark theme by default in Terminal.app${C_DEFAULT}\n"
# osascript <<EOD

# tell application "Terminal"

# 	local allOpenedWindows
# 	local initialOpenedWindows
# 	local windowID
# 	set themeName to "Solarized Dark xterm-256color"

# 	(* Store the IDs of all the open terminal windows. *)
# 	set initialOpenedWindows to id of every window

# 	(* Open the custom theme so that it gets added to the list
# 	   of available terminal themes (note: this will open two
# 	   additional terminal windows). *)
# 	do shell script "open '$HOME/init/" & themeName & ".terminal'"

# 	(* Wait a little bit to ensure that the custom theme is added. *)
# 	delay 1

# 	(* Set the custom theme as the default terminal theme. *)
# 	set default settings to settings set themeName

# 	(* Get the IDs of all the currently opened terminal windows. *)
# 	set allOpenedWindows to id of every window

# 	repeat with windowID in allOpenedWindows

# 		(* Close the additional windows that were opened in order
# 		   to add the custom theme to the list of terminal themes. *)
# 		if initialOpenedWindows does not contain windowID then
# 			close (every window whose id is windowID)

# 		(* Change the theme for the initial opened terminal windows
# 		   to remove the need to close them in order for the custom
# 		   theme to be applied. *)
# 		else
# 			set current settings of tabs of (every window whose id is windowID) to settings set themeName
# 		end if

# 	end repeat

# end tell

# EOD

printf "${C_CYAN}Terminal: Enable “focus follows mouse” for Terminal.app and all X11 apps${C_DEFAULT}\n"
# i.e. hover over a window and start typing in it without clicking first
#defaults write com.apple.terminal FocusFollowsMouse -bool true
#defaults write org.x.X11 wm_ffm -bool true

printf "${C_CYAN}Terminal: Enable Secure Keyboard Entry in Terminal.app${C_DEFAULT}\n"
# See: https://security.stackexchange.com/a/47786/8918
defaults write com.apple.terminal SecureKeyboardEntry -bool true

printf "${C_CYAN}Terminal: Disable the annoying line marks${C_DEFAULT}\n"
defaults write com.apple.Terminal ShowLineMarks -int 0

# printf "${C_CYAN}Install the Solarized Dark theme for iTerm${C_DEFAULT}\n"
# open "${HOME}/init/Solarized Dark.itermcolors"

printf "${C_CYAN}Terminal: Don’t display the annoying prompt when quitting iTerm${C_DEFAULT}\n"
defaults write com.googlecode.iterm2 PromptOnQuit -bool false

###############################################################################
# Mac App Store                                                               #
###############################################################################

printf "${C_CYAN}App Store: Enable the WebKit Developer Tools in the Mac App Store${C_DEFAULT}\n"
defaults write com.apple.appstore WebKitDeveloperExtras -bool true

printf "${C_CYAN}App Store: Enable Debug Menu in the Mac App Store${C_DEFAULT}\n"
defaults write com.apple.appstore ShowDebugMenu -bool true

printf "${C_CYAN}App Store: Enable the automatic update check${C_DEFAULT}\n"
defaults write com.apple.SoftwareUpdate AutomaticCheckEnabled -bool true

printf "${C_CYAN}App Store: Check for software updates daily, not just once per week${C_DEFAULT}\n"
defaults write com.apple.SoftwareUpdate ScheduleFrequency -int 1

printf "${C_CYAN}App Store: Download newly available updates in background${C_DEFAULT}\n"
defaults write com.apple.SoftwareUpdate AutomaticDownload -int 1

printf "${C_CYAN}App Store: Install System data files & security updates${C_DEFAULT}\n"
defaults write com.apple.SoftwareUpdate CriticalUpdateInstall -int 1

printf "${C_CYAN}App Store: Automatically download apps purchased on other Macs${C_DEFAULT}\n"
defaults write com.apple.SoftwareUpdate ConfigDataInstall -int 1

printf "${C_CYAN}App Store: Turn on app auto-update${C_DEFAULT}\n"
defaults write com.apple.commerce AutoUpdate -bool true

printf "${C_CYAN}App Store: Allow the App Store to reboot machine on macOS updates${C_DEFAULT}\n"
defaults write com.apple.commerce AutoUpdateRestartRequired -bool true

###############################################################################
# Google Chrome                                                               #
###############################################################################

printf "${C_CYAN}Chrome: Disable the all too sensitive backswipe on trackpads${C_DEFAULT}\n"
defaults write com.google.Chrome AppleEnableSwipeNavigateWithScrolls -bool false

printf "${C_CYAN}Chrome: Disable the all too sensitive backswipe on Magic Mouse${C_DEFAULT}\n"
defaults write com.google.Chrome AppleEnableMouseSwipeNavigateWithScrolls -bool false

printf "${C_CYAN}Chrome: Use the system-native print preview dialog${C_DEFAULT}\n"
defaults write com.google.Chrome DisablePrintPreview -bool true

printf "${C_CYAN}Chrome: Expand the print dialog by default${C_DEFAULT}\n"
defaults write com.google.Chrome PMPrintingExpandedStateForPrint2 -bool true

printf "\n\n${C_LIGHTGREEN}Initial build complete. Please run the Step 2 Applications script.${C_DEFAULT}\n"
printf "\n"

